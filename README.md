# WebSocketForWebAPI

#### 项目介绍
WebAPI版本的websocket程序

#### 软件架构
- .Net4.5+
- IIS8+


#### 使用说明

- 包含两个文件，一个cshtml，一个WebAPI的控制器。
- 请通过在程序中添加控制器再复制代码的方式使用，直接将文件复制无效
- 不知道怎么访问页面的请自行学习ASP.Net MVC相关知识


